![Open Aristos](https://openaristos.io/assets/img/brands/oa/logo.png)

# Open Aristos
Open Aristos the fastest way to build a modern data platform in minutes.

If you're interested in learning more then read the documentation: https://docs.openaristos.io/

# Building and Testing
Open Aristos uses Maven and requires Java 1.8.0_40+ for proper building and proper operations. To build, execute unit tests and package Open Aristos Java SDK run:

```bash
mvn clean install
```

## Contributions

Copyright (C) 2020 Aristos Data, LLC

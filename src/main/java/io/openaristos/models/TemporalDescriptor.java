package io.openaristos.models;

import com.google.common.collect.*;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Map;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

@SuppressWarnings("UnstableApiUsage")
@EqualsAndHashCode
public class TemporalDescriptor implements Serializable {
  private String key;
  private String value;
  private Map<Long, Long> effectiveDating;

  public TemporalDescriptor() {
  }

  private TemporalDescriptor(String key, String value, RangeSet<Long> effectiveDating) {
    this.key = key;
    this.value = value;
    this.effectiveDating = Maps.newHashMap();

    effectiveDating.asRanges().forEach(r -> {
      this.effectiveDating.put(r.lowerEndpoint(), r.upperEndpoint());
    });

  }

  public static TemporalDescriptor of(String key, String value, RangeSet<Long> effectiveDating) {
    checkNotNull(key);
    checkNotNull(value);
    checkNotNull(effectiveDating);

    return new TemporalDescriptor(key, value, effectiveDating);
  }

  public static TemporalDescriptor of(String key, String value, long effectiveStartDt, long effectiveEndDt) {
    checkNotNull(key);
    checkNotNull(value);
    checkArgument(effectiveStartDt >= 0);
    checkArgument(effectiveEndDt > effectiveStartDt);

    return new TemporalDescriptor(key, value, TreeRangeSet.create(ImmutableSet.of(Range.closed(effectiveStartDt, effectiveEndDt))));
  }

  public static TemporalDescriptor of(String key, String value, long effectiveStartDt) {
    checkNotNull(key);
    checkNotNull(value);
    checkArgument(effectiveStartDt >= 0);
    return new TemporalDescriptor(key, value, TreeRangeSet.create(ImmutableSet.of(Range.closed(effectiveStartDt, Long.MAX_VALUE))));
  }

  public static TemporalDescriptor of(String key, String value, Timestamp effectiveStartDt, Timestamp effectiveEndDt) {
    checkNotNull(key);
    checkNotNull(value);
    checkNotNull(effectiveStartDt);
    checkNotNull(effectiveEndDt);

    return new TemporalDescriptor(key, value, TreeRangeSet.create(ImmutableSet.of(Range.closed(effectiveStartDt.getTime() / 1000, effectiveEndDt.getTime() / 1000))));
  }

  public static TemporalDescriptor of(String key, String value) {
    checkNotNull(key);
    checkNotNull(value);

    return new TemporalDescriptor(key, value, TreeRangeSet.create(ImmutableSet.of(Range.closed(0L, Long.MAX_VALUE))));
  }

  public String getKey() {
    return key;
  }

  public String getValue() {
    return value;
  }

}

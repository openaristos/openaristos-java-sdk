package io.openaristos.models;

import com.google.common.collect.*;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Map;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

@SuppressWarnings("UnstableApiUsage")
@EqualsAndHashCode
public class Relationship implements Knowledge, Serializable {
  private Entity sourceEntity;
  private Entity targetEntity;
  private String relationshipType;
  private Map<Long, Long> effectiveDating;


  public Relationship() {

  }

  private Relationship(Entity sourceEntity, Entity targetEntity, String relationshipType, RangeSet<Long> effectiveDating) {
    this.sourceEntity = sourceEntity;
    this.targetEntity = targetEntity;
    this.relationshipType = relationshipType;

    this.effectiveDating = Maps.newHashMap();

    effectiveDating.asRanges().forEach(r -> {
      this.effectiveDating.put(r.lowerEndpoint(), r.upperEndpoint());
    });

  }

  public static Relationship of(Entity sourceEntity, Entity targetEntity, String relationshipType) {
    checkNotNull(sourceEntity);
    checkNotNull(targetEntity);
    checkNotNull(relationshipType);

    return new Relationship(sourceEntity, targetEntity, relationshipType, TreeRangeSet.create(
        ImmutableSet.of(Range.closed(0L, Long.MAX_VALUE))));
  }

  public static Relationship of(Entity sourceEntity, Entity targetEntity, String relationshipType, long effectiveStartDt) {
    checkNotNull(sourceEntity);
    checkNotNull(targetEntity);
    checkNotNull(relationshipType);
    checkArgument(effectiveStartDt >= 0);

    return new Relationship(sourceEntity, targetEntity, relationshipType, TreeRangeSet.create(ImmutableSet.of(
        Range.closed(effectiveStartDt, Long.MAX_VALUE))));
  }


  public static Relationship of(Entity sourceEntity, Entity targetEntity, String relationshipType, Timestamp effectiveStartDt, Timestamp effectiveEndDt) {
    checkNotNull(sourceEntity);
    checkNotNull(targetEntity);
    checkNotNull(relationshipType);
    checkNotNull(effectiveStartDt);
    checkNotNull(effectiveEndDt);

    return new Relationship(sourceEntity, targetEntity, relationshipType, TreeRangeSet.create(ImmutableSet.of(
        Range.closed(effectiveStartDt.getTime() / 1000, effectiveEndDt.getTime() / 1000))));
  }


  public static Relationship of(Entity sourceEntity, Entity targetEntity, String relationshipType, RangeSet<Long> effectiveDating) {
    checkNotNull(sourceEntity);
    checkNotNull(targetEntity);
    checkNotNull(relationshipType);
    checkNotNull(effectiveDating);

    return new Relationship(sourceEntity, targetEntity, relationshipType, effectiveDating);
  }

}

package io.openaristos.models;

import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Map;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

@EqualsAndHashCode
public class Asset implements Knowledge, Serializable {
  private String resourceCode;
  private Map<String, Entity> retrievedBy;
  private Map<String, Object> fields;
  private long effectiveStartDt;
  private long effectiveEndDt;

  public Asset() {

  }

  public Asset(String resourceCode, Map<String, Entity> retrievedBy, Map<String, Object> fields, long effectiveStartDt, long effectiveEndDt) {
    this.resourceCode = resourceCode;
    this.retrievedBy = retrievedBy;
    this.fields = fields;
    this.effectiveStartDt = effectiveStartDt;
    this.effectiveEndDt = effectiveEndDt;
  }

  public static Asset of(String resourceCode, Map<String, Entity> retrievedBy, Map<String, Object> fields, long effectiveStartDt, long effectiveEndDt) {
    checkNotNull(resourceCode);
    checkNotNull(retrievedBy);
    checkNotNull(fields);
    checkArgument(effectiveStartDt > 0);
    checkArgument(effectiveEndDt > effectiveStartDt);

    return new Asset(resourceCode, retrievedBy, fields, effectiveStartDt, effectiveEndDt);
  }

  public String getResourceCode() {
    return resourceCode;
  }

  public Map<String, Entity> getRetrievedBy() {
    return retrievedBy;
  }

  public Map<String, Object> getFields() {
    return fields;
  }

  public long getEffectiveStartDt() {
    return effectiveStartDt;
  }

  public long getEffectiveEndDt() {
    return effectiveEndDt;
  }
}

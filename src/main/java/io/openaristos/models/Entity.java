package io.openaristos.models;

import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Set;

@EqualsAndHashCode
public class Entity implements Knowledge, Serializable {
  private Identity identity;

  @EqualsAndHashCode.Exclude
  private String type;


  @EqualsAndHashCode.Exclude
  private TemporalDescriptor[] descriptors;

  public Entity() {

  }

  private Entity(String type, Identity identity, Set<TemporalDescriptor> descriptors) {
    this.identity = identity;
    this.type = type;
    this.descriptors = descriptors.toArray(new TemporalDescriptor[]{});
  }


  public static Entity of(String type, Identity identity, Set<TemporalDescriptor> descriptors) {
    return new Entity(type, identity, descriptors);
  }

  public Identity getIdentity() {
    return identity;
  }

  public String getType() {
    return type;
  }

  public TemporalDescriptor[] getDescriptors() {
    return descriptors;
  }

}

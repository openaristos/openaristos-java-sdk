package io.openaristos.models;

import com.google.common.hash.Hashing;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.util.Set;
import java.util.stream.Collectors;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

@SuppressWarnings("UnstableApiUsage")
@EqualsAndHashCode
public class Identity implements Serializable {
  private String identity;
  private String perspective;
  private Descriptor[] descriptors;

  private Identity(String perspective, Set<Descriptor> descriptors) {
    checkArgument(descriptors.stream().noneMatch(x -> x.getValue() == null));

    this.perspective = perspective;
    this.descriptors = descriptors.toArray(new Descriptor[]{});
    this.identity = Identity.createUid(perspective, descriptors);
  }

  public static String createUid(String perspective, Set<Descriptor> descriptors) {
    checkNotNull(perspective);
    checkNotNull(descriptors);

    final String toSign =
        perspective + String.join(",", descriptors.stream().map(x -> String.format("%s:%s", x.getKey(), x.getValue())).sorted().collect(Collectors.toList()));

    return Hashing.sha256().hashString(toSign, StandardCharsets.UTF_8).toString();
  }

  public static Identity of(String perspective, Set<Descriptor> descriptors) {
    checkNotNull(perspective);
    checkNotNull(descriptors);
    checkArgument(descriptors.size() > 0);

    return new Identity(perspective, descriptors);
  }

  public String getIdentity() {
    return identity;
  }

  public String getPerspective() {
    return perspective;
  }

  public Descriptor[] getDescriptors() {
    return descriptors;
  }
}

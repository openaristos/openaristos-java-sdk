package io.openaristos.models;

import lombok.EqualsAndHashCode;

import java.io.Serializable;


@EqualsAndHashCode
public class Descriptor implements Serializable {
  private String key;
  private String value;

  public Descriptor() {

  }

  protected Descriptor(String key, String value) {
    this.key = key;
    this.value = value;
  }

  public static Descriptor of(String key, String value) {
    return new Descriptor(key, value);
  }

  public String getKey() {
    return key;
  }

  public String getValue() {
    return value;
  }


}
